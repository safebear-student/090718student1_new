pipeline {
    agent any

    parameters {
        //tests to run
        //API
        string (name:'apitests',defaultValue:'ApiTestsIT',description:'API Tests')
        //Cucumber
        string (name:'cuke',defaultValue:'RunCukesIT',description:'cucumber Tests')

        //general
        string (name:'context',defaultValue:'safebear',description:'application context')
        string (name:'domain',defaultValue:'http://34.209.136.119',description:'application context')

        //test environment
        string (name:'test_hostname',defaultValue:'34.209.136.119',description:'hostname of the test environment')
        string (name:'test_port',defaultValue:'8888',description:'port of the test environment')
        string (name:'test_username',defaultValue:'tomcat',description:'username of the test environment')
        string (name:'test_password',defaultValue:'tomcat',description:'password of the test environment')

        //chrome headless
        string(name: 'browser', defaultValue: 'headless',description:'browser for our gui tests')
    }

    options {
        buildDiscarder (logRotator(numToKeepStr:'3',artifactNumToKeepStr:'3')) // number of logs/builds to keep
    }

    triggers {
        pollSCM('H/1 * * * *')  //poll every 5mins, H/5 means wait for any builds to complete

    }

    stages  {
        stage ('Build with unit tests') {
            steps {
                sh 'mvn clean package sonar:sonar -Dsonar.organization=safebear-student-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=f08a36f3598527bf9b20d9b861b57c0aaa5937e3'
                //sh for linux, bat for windows
            }

            post {
                /* only run if the lat step succeeds*/

                success {
                    //print a message to the screen that we are archiving
                    echo 'Now archiving...'
                    //archive the artifacts so we can build once and then use them later to deploy
                    archiveArtifacts artifacts:'**/target/*.war'
                }

                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static Analysis') {

                /* run the mvn checkstyle;checkstyle command to run the static analysis.
        can be done in parallel to the build and unit
        testing step
         */
                steps {
                    sh 'mvn checkstyle:checkstyle'
                }

                post {
                    success {
                        checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                    }
                }

            }

        stage('deployment to test') {
                steps {
                    sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
                }
            }

        stage ('Integration Test') {
            steps {
                sh "mvn -Dtest=${params.apitests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApiTestsIT.xml"
                }
            }
        }

        stage ('BDD Requirements Testing'){
            steps {
                //sh "mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"
                sh "mvn -Dtest=\"${params.cuke}\" verify -Ddomain=\"${params.domain}\" -Dport=${params.test_port} -Dcontext=\"${params.context}\" -Dbrowser=\"${params.browser}\""
            }
            post{
                always{
                    publishHTML([
                            allowMissing:false,
                            alwaysLinkToLastBuild:false,
                            keepAll:false,
                            reportDir:'target/cucumber',
                            reportFiles:'extent_report.html',
                            //reportFiles:'index.html',
                            reportName:'BDD Report',
                            reportTitles:''])
                }
            }
        }
}
}