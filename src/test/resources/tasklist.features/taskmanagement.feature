Feature: Task management

  User Story:
  In order to manage my tasks
  As a user
  I must be able to view, add,archive or update tasks.

  Rules:
  - You must be able to view all tasks
  - They must be paginated
  - You must be ale to see the name, due date and status of a task
  - You must be able to change status of a task
  - You must be able to archive tasks

  Questions:
  - How many tasks per page for the pagination?
  - Where do you archive to?
  - Is the due date configurable or should it be hard-coded to a certain
  number of days in the future

  To do:
  - Pagination
  - Archiving

  Domain language:
  - Task = this is made up of a description, a status and a due date.
  - Due date = this must be in the format dd/MM/YYYY
  - Status = this is 'completed' or 'not completed'
  - Task List = this is a list of tasks and can be displayed on the home page.

#  Background:
#    Given the following tasks are created
#      |cooking|
#      |gardening|

 #to-do will skip the tests, each test can have multiple tags

  Scenario Outline: A user creates a task
    When a user creates a <task> task
    Then the <task> task appears in the list
    Examples:
      |task|
      |laundry|
      |Gardening|
      |cooking  |
#@to-do
#  Scenario: A user opens the home page
#    When the homepage opens
#    Then the following tasks appear in the list:
#      |cooking|
#      |gardening|
#@to-do
#    Scenario Outline: A user changes the state of a task
#      Given a <task> task is in a <startingstate>
#      When a <task> task is updated to a <endstate>
#      Then the <task> task is now in the <endstate>
#      Examples:
#      |startingstate|task|endstate|
#      |not completed|cooking|completed|
#      |not completed|gardening|completed|