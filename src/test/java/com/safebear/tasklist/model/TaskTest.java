package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    @Test
    public void creation(){
        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L,"study",localDate,false);
        Assertions.assertThat(task.getID()).isEqualTo(1L);
        Assertions.assertThat(task.getName()).isNotBlank();
        Assertions.assertThat(task.getName()).isEqualTo("study");
        Assertions.assertThat(task.getDueDate()).isEqualTo(localDate);
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);
    }
}
